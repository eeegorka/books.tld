<?php

use Illuminate\Database\Seeder;

class FakeUsersAndBooks extends Seeder
{
    /**
     * Run the database seeds.
     * Factory for create User with relation books()
     * this function call in namespace App\Console\Commands\loadFakeData
     * @return void
     */
    public function run()
    {
        factory(App\User::class,50)->create()->each(function ($user) {
            $user->books()->save(factory(App\Book::class)->make());
        });
    }
}
